#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <stack>

#include "TTextLink.h"

class TText {
public:
    TText(TTextLink* pl = nullptr);
    ~TText(void);
    TText* GetCopy(void);

    // Навигация
    void GoFirstLink(void); // переход к первой строке
    void GoDownLink(void);  // переход к следующей строке по Down
    void GoNextLink(void);  // переход к следующей строке по Next
    void GoPrevLink(void);  // переход к предыдущей позиции в тексте

    // Доступ
    std::string GetLine(void); // чтение текущей строки
    void SetLine(std::string); // замена текущей строки

    // Модификация
    void InsDownLine(std::string);    // вставка строки в подуровень
    void InsDownSection(std::string); // вставка раздела в подуровень
    void InsNextLine(std::string);    // вставка строки в том же уровне
    void InsNextSection(std::string); // вставка раздела в том же уровне
    void DelDownLine(void);    // удаление строки в подуровне
    void DelDownSection(void); // удаление раздела в подуровне
    void DelNextLine(void);    // удаление строки в том же уровне
    void DelNextSection(void); // удаление раздела в том же уровне

    // Итератор
    void Reset(void);             // установить на первую звапись
    bool IsTextEnded(void) const; // текст завершен?
    void GoNext(void);            // переход к следующей записи

    // Работа с файлами
    void Read(char* pFileName);  // ввод текста из файла
    void Write(char* pFileName); // вывод текста в файл

    // Печать текста
    void Print(void);

protected:
    TTextLink* pFirst_;   // указатель корня дерева
    TTextLink* pCurrent_; // указатель текущей строки
    std::stack<TTextLink*> path_; // стек траектории движения по тексту
    std::stack<TTextLink*> st_;   // стек для итератора

    TTextLink* GetFirstAtom(TTextLink* pl); // поиск первого атома
    void PrintText(TTextLink* ptl);         // печать текста со звена ptl
    void PrintTextF(TTextLink* ptl, std::ofstream& textFile); // печать текста со звена ptl в файл
    TTextLink* ReadText(std::ifstream& txtFile); // чтение текста из файла
};
